<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Silence is golden.
}
?>
<?php get_header(); ?>

<div class="fl-archive <?php FLLayout::container_class(); ?>">
<div class="<?php FLLayout::row_class(); ?>">
<?php 
 if ( is_front_page() && is_home() ){
	// Default homepage
} elseif ( is_front_page()){
	//Static homepage
} elseif ( is_home()){
	?>
	<div class="blog_head_wrap">
	<h1>Our Blog</h1>
	<p>Filter our blog articles by theme, or peruse theme all!</p>
	</div>
	<?php 
}else{

}
?>

</div>
	<div class="<?php FLLayout::row_class(); ?>">

		<?php FLTheme::sidebar( 'left' ); ?>

		<div class="fl-content <?php FLLayout::content_class(); ?>"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/Blog"' ); ?>>


			<?php FLTheme::archive_page_header(); ?>

			<?php if ( have_posts() ) : ?>

				<?php
				while ( have_posts() ) :
					the_post();
					?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>

				<?php FLTheme::archive_nav(); ?>

			<?php else : ?>

				<?php get_template_part( 'content', 'no-results' ); ?>

			<?php endif; ?>

		</div>

		<?php FLTheme::sidebar( 'right' ); ?>

	</div>
</div>

<?php get_footer(); ?>
